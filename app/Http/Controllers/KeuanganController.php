<?php

namespace App\Http\Controllers;

use DB;
use App\Keuangan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class KeuanganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $keuangan = Keuangan::orderBy('waktu', 'DESC')->paginate(10);
        return $keuangan;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $operator = $request->tipe == 'Pengeluaran' ? '-' : '+';
        $tanggal = $request->waktu ? Carbon::parse($request->waktu)->format('y-m-d h:i:s') : '';
        $keuangan = Keuangan::create([
            'jumlah' => $request->jumlah,
            'keterangan' => $request->keterangan, 
            'operator' => $operator,
            'waktu' => $tanggal
        ]);
        return response($keuangan->jsonSerialize(), Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $keuangan = Keuangan::find($id);
        return $keuangan;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $operator = $request->tipe == 'Pengeluaran' ? '-' : '+';
        $tanggal = $request->waktu ? Carbon::parse($request->waktu)->format('y-m-d h:i:s') : '';

        $keuangan = Keuangan::find($id);
        $keuangan->jumlah = $request->jumlah;
        $keuangan->keterangan = $request->keterangan;
        $keuangan->operator = $operator;
        $keuangan->waktu = $tanggal;
        $keuangan->save();
        return $keuangan;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function laporan(Request $request)
    {
        $keuangan = DB::table('keuangan')->whereMonth('created_at', '=', '12')->get();
        $total = 0;
        foreach ($keuangan as $key => $value) {
            if ($value->operator == '+') {
                $total += $value->jumlah;
            } else {
                $total -= $value->jumlah;
            }
        }
        return $total;
    }
}
