<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keuangan extends Model
{
    protected $table = 'keuangan';

    protected $fillable = [
    	'jumlah', 'operator', 'keterangan', 'waktu'
    ];

    public function getOperatorAttribute() {
    	return $this->attributes['operator'] == '+' ? 'Pemasukan' : 'Pengeluaran';
    }

    public function getWaktuAttribute() {
    	return \Carbon\Carbon::parse($this->attributes['waktu'])->toIso8601String();
    }
}
