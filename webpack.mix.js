const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.styles([
    'public/css/uikit.min.css',
], 'public/css/uikit-compile.css');
mix.js([
	'public/js/uikit.min.js',
], 'public/js/uikit-compile.js');
mix.js('resources/js/app.js', 'public/js')