<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <title>Vue #1 First Day</title>

    <link rel="stylesheet" href="/css/uikit-compile.css">
    <style>
        html {
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }
        .uk-label {
            font-size: 0.725rem;
        }
        h3 {
            font-weight: 600;
        }
        .uk-container-small {
            max-width: 48em;
        }
    </style>
</head>
<body>
    <div class="uk-container uk-container-small uk-section">
        <div id="app"></div>
        <!-- <a href="#" class="uk-button uk-button-secondary">GIVE ME BATU BATA! - IT'S FREE</a> -->
    </div>
    <script src="/js/app.js"></script>
    <script src="/js/uikit-compile.js"></script>
</body>
</html>
