import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';
import VueAxios from 'vue-axios';
import Paginate from 'vuejs-paginate';
import VueCurrencyFilter from 'vue-currency-filter';
import App from './App.vue';
import HomePage from './components/ExampleComponent.vue';
import CreatePage from './components/CreateComponent.vue';
import EditPage from './components/EditComponent.vue';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to 
 fit your unique needs.
 */
const moment = require('moment')
require('moment/locale/id')

Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.component('paginate', Paginate);
Vue.use(require('vue-moment'), {
    moment
});
Vue.use(VueCurrencyFilter,
{
  symbol : 'Rp',
  thousandsSeparator: '.',
  fractionCount: 0,
  fractionSeparator: ',',
  symbolPosition: 'front',
  symbolSpacing: true
});

Vue.prototype.$axios = axios
axios.defaults.baseURL = 'http://localhost:8000/api'

const router = new VueRouter({
	routes: [
		{
            path: '/',
            name: 'home',
            component: HomePage
		},
		{
            path: '/create',
            name: 'create',
            component: CreatePage
		},
    {
            path: '/edit/:id',
            name: 'edit',
            component: EditPage
    }
	]
});

Vue.router = router
App.router = Vue.router

// const app = new Vue({
//     el: '#app'
// });

new Vue(App).$mount('#app');